ARG PYTHON="3.9"
ARG POETRY_VERSION="1.1.12"
ARG IMAGE_TAG="${PYTHON}-slim-buster"

FROM python:${IMAGE_TAG} as builder

ENV DEBIAN_FRONTEND=noninteractive

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get -y install --no-install-recommends \
    build-essential \
    libglib2.0-dev \
    libssl-dev \
    git

COPY requirements.txt .
RUN pip install -r requirements.txt

FROM python:${IMAGE_TAG}

ARG PYTHON

# hadolint ignore=DL3008
RUN apt-get update \
    && apt-get -y install --no-install-recommends \
    libglib2.0 \
    bluez \
    rfkill \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/lib/python${PYTHON}/site-packages /usr/local/lib/python${PYTHON}/site-packages

RUN mkdir /opt/humpuuki
RUN mkdir /etc/humpuuki

COPY . /opt/humpuuki

WORKDIR /opt/humpuuki

# hadolint ignore=DL3025
ENTRYPOINT [ "/opt/humpuuki/humpuuki.py" ]
CMD []
