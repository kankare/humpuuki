import os
from unittest.mock import MagicMock, call, patch

import pytest

from config.base import HumpuukiConfig


def test_config_devices(monkeypatch):
    environ = {"HUMPUUKI_CONFIG_FILE": "./test/fixtures/test-config-devices.yaml"}

    for k, v in environ.items():
        monkeypatch.setenv(k, v)

    c = HumpuukiConfig()

    assert c.devices()[0].manufacturer == "Test"
    assert c.devices()[0].model == "RuuviTag"
    assert c.devices()[0].mqtt.get("publish_availability") == False

    assert c.devices()[1].manufacturer == "Test"
    assert c.devices()[1].model == "RuuviTag"
    assert c.devices()[1].mqtt.get("publish_availability") == True

    assert c.devices()[2].manufacturer == "Xiaomi"
    assert c.devices()[2].model == "Test"
    assert c.devices()[2].logger.level == "debug"

    assert c.devices()[3].manufacturer == "Xiaomi"
    assert c.devices()[3].model == "Test"
    assert c.devices()[3].logger.level == "warning"


@patch("paho.mqtt.client.Client")
def test_config_backends(mock_mqtt, monkeypatch):
    environ = {"HUMPUUKI_CONFIG_FILE": "./test/fixtures/test-config-backends.yaml"}

    for k, v in environ.items():
        monkeypatch.setenv(k, v)

    c = HumpuukiConfig()

    assert c.backends()[0].host == "127.0.0.1"
    assert c.backends()[0].port == 3881

    assert c.backends()[1].level == "DEBUG"


def test_config_environment(monkeypatch):
    environ = {
        "HUMPUUKI_CONFIG_FILE": "./test/fixtures/test-config-backends.yaml",
        "humpuuki_logging__level": "debug",
        "humpuuki_ruuvi__name": "foo",
        "humpuuki_ruuvi__devices": '[{"address":"CA:FE:CA:FE:CA:FE"}]',
        "humpuuki_mqtt__host": "127.0.0.1",
        "humpuuki_mqtt__port": "8331",
    }

    for k, v in environ.items():
        monkeypatch.setenv(k, v)

    c = HumpuukiConfig()

    assert c.logging.level == "debug"
    assert c.mqtt.host == "127.0.0.1"
    assert c.mqtt.port == 8331
    assert c.devices()[0].model == "RuuviTag"
    assert c.devices()[0].name == "foo"
    assert c.devices()[0].address == "CA:FE:CA:FE:CA:FE"


def test_config_all(monkeypatch):
    environ = {
        "HUMPUUKI_CONFIG_FILE": "./test/fixtures/test-config-all.yaml",
        "HUMPUUKI_LOGGING__LEVEL": "debug",
        "humpuuki_mqtt__port": "8331",
    }

    for k, v in environ.items():
        monkeypatch.setenv(k, v)

    c = HumpuukiConfig()

    assert c.logging.level == "debug"
    assert c.mqtt.host == "test.example.com"
    assert c.mqtt.port == 8331

    assert c.devices()[0].manufacturer == "Test"
    assert c.devices()[0].model == "RuuviTag"
    assert c.devices()[0].mqtt.get("publish_availability") == False

    assert c.devices()[1].manufacturer == "Test"
    assert c.devices()[1].model == "RuuviTag"
    assert c.devices()[1].mqtt.get("publish_availability") == True

    assert c.devices()[2].manufacturer == "Xiaomi"
    assert c.devices()[2].model == "Test"
    assert c.devices()[2].logger.level == "debug"

    assert c.devices()[3].manufacturer == "Xiaomi"
    assert c.devices()[3].model == "Test"
    assert c.devices()[3].logger.level == "warning"
