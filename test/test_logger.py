import logging
from unittest.mock import MagicMock, call, patch

from backend.logger import LoggerBackend
from device.miflora import MiFloraDevice
from device.ruuvi import RuuviDevice

log = logging.getLogger("humpuuki")


def test_logger_config():
    logger = LoggerBackend(level="DEBUG")
    logger.configure()

    assert logger._output == log.debug


@patch.object(log, "info", MagicMock())
def test_logger_publish():
    logger = LoggerBackend(level="info")
    logger.configure()

    plant = MiFloraDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    plant.battery.value = 3
    plant.temperature.value = 31.2
    plant.conductivity.value = 12345

    ruuvitag = RuuviDevice(name="test", address="CA:FE:CA:FE:CA:FE")

    ruuvitag.battery_voltage.value = 2.345
    ruuvitag.temperature.value = 12.3
    ruuvitag.movement_counter.value = 123

    logger.publish([plant, ruuvitag])

    log.info.assert_has_calls([call(f"Data: {plant}"), call(f"Data: {ruuvitag}")])
