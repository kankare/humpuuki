# type: ignore[attr-defined]

from typing import Optional

from .base import HumpuukiConfig

_c: Optional[HumpuukiConfig] = None


def __getattr__(name: str) -> HumpuukiConfig:
    if name == "c":
        global _c
        if not _c:
            _c = HumpuukiConfig()
        return _c
    raise AttributeError(f"module {__name__!r} has no attribute {name!r}")
