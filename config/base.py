# type: ignore[attr-defined]

from __future__ import annotations

import argparse
import json
import logging
import os
from pathlib import Path
from typing import Any, Dict, List, Optional

from addict import Dict as Addict
from climatecontrol import Climate
from climatecontrol.utils import merge_nested
from pydantic import BaseModel

import backend.base
import backend.logger
import backend.mqtt
import device.base
import device.miflora
import device.ruuvi

log = logging.getLogger("humpuuki")


class Logging(BaseModel):
    level: str = "info"
    format: str = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"


# FIXME: These configuration parameters should be sorted out
# with regards to the device specific ones. Nothing except
# timeout is used.
class Scan(BaseModel):
    interval: int = 60
    timeout: int = 10
    retries: int = 3
    cache_time: int = 180


class RuuviDevices(BaseModel):
    devices: Optional[List[device.ruuvi.RuuviDevice]] = None


class MiFloraDevices(BaseModel):
    devices: Optional[List[device.miflora.MiFloraDevice]] = None


class ConfigSchema(BaseModel):
    config_file: str = "/etc/humpuuki/config.yml"
    check_mode: bool = False

    logging: Logging = Logging()
    scan: Scan = Scan()

    ruuvi: Optional[RuuviDevices] = RuuviDevices()
    miflora: Optional[MiFloraDevices] = MiFloraDevices()

    mqtt: Optional[backend.mqtt.MqttBackend] = None
    logger: Optional[backend.logger.LoggerBackend] = None


class HumpuukiConfig(ConfigSchema):
    def __init__(self) -> None:
        parser = argparse.ArgumentParser()
        parser.add_argument("-c", "--config-file", type=str)
        args = parser.parse_known_args()

        env = Climate(prefix="HUMPUUKI")
        env = Addict(env.settings)

        config_file = env.config_file

        if args[0].config_file:
            config_file = args[0].config_file

        try:
            config = Climate(settings_files=[config_file])
            config = Addict(config.settings)
            config_file_read = True
        except:
            config_file_read = False
            config = Addict()

        config.update(env)

        if config.ruuvi:
            shared = config.ruuvi.copy()
            shared.pop("devices")
            for i, device in enumerate(config.ruuvi.devices):
                config.ruuvi.devices[i] = config.ruuvi | device

        if config.miflora:
            shared = config.miflora.copy()
            shared.pop("devices")
            for i, device in enumerate(config.miflora.devices):
                config.miflora.devices[i] = config.miflora | device

        super().__init__(**config)

        console = logging.StreamHandler()
        log.addHandler(console)

        formatter = logging.Formatter(self.logging.format)
        log.setLevel(self.logging.level.upper())

        console.setFormatter(formatter)

        if not config_file_read:
            log.warning(f"Could not read file: {config_file}")

        log.debug(f"Configuration: {self.dict()}")

    # @property
    # FIXME: Pydantic and @property don't play well together
    #        See: https://github.com/samuelcolvin/pydantic/issues/935
    def devices(self) -> List[device.base.HumpuukiDevice]:
        devices = [x.devices for x in [self.ruuvi, self.miflora] if x and x.devices]
        return [d for x in devices for d in x]

    # @property
    def backends(self) -> List[backend.base.HumpuukiBackend]:
        return [x for x in [self.mqtt, self.logger] if x]


RuuviDevices.update_forward_refs()
MiFloraDevices.update_forward_refs()
